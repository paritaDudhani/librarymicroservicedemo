const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

app.use(bodyParser.json());

require('./customer.model.js');
const Customer = mongoose.model('customers');

mongoose.connect('mongodb+srv://parita-d:mongodbb@cluster0-bfv8j.mongodb.net/CustomerService?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
        console.log('Connection established with CustomerService');
    }).catch(() => {
        console.log('Connection failed');
    });

app.post('/customer', (req, res) => {
    const customer = new Customer(req.body);
    customer.save().then((data) => {
        if (data) {
            res.status(200).send(data);
        } else {
            res.status(400).send('Something went wrong');
        }
    }).catch((err) => {
        res.send(err.message);
    });
});

app.get('/customers', (req, res) => {
    Customer.find().then((customers) => {
        if (customers) {
            res.status(200).send(customers);
        } else {
            res.status(404).send('No customers found');
        }
    }).catch(err => res.send(err.message));
});

app.get('/customer/:id', (req, res) => {
    Customer.findById(req.params.id).then((customer) => {
        if (customer) {
            res.status(200).send(customer);
        } else {
            res.status(404).send('No customer found');
        }
    }).catch(err => res.send(err.message));
});

app.delete('/customer/:id', (req, res) => {
    Customer.findByIdAndDelete(req.params.id).then((customer) => {
        if (customer) {
            res.status(200).send(customer);
        } else {
            res.status(404).send('No customer found');
        }
    }).catch(err => res.send(err.message));
});

app.listen(4000, () => {
    console.log('Running customer service');
});