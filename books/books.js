const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('./book.model.js');
const Book = mongoose.model('books');

app.use(bodyParser.json());

mongoose.connect('mongodb+srv://parita-d:mongodbb@cluster0-bfv8j.mongodb.net/Books?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
        console.log('Connection established with Book');
    }).catch(() => {
        console.log('Connection failed');
    });

app.get('/', (req, res) => {
    res.send('This is our main endpoint');
});

app.post('/book', (req, res) => {
    const book = new Book(req.body);
    book.save().then((data) => {
        res.status(200).send(data);
    }).catch((err) => {
        res.status(400).send(err);
    });
});

app.get('/books', (req, res) => {
    Book.find().then((data) => {
        res.status(200).send(data);
    }).catch((err) => {
        res.status(400).send(err);
    });
});

app.get('/book/:id', (req, res) => {
    Book.findById(req.params.id).then((book) => {
        if (book) {
            res.status(200).send(book);
        } else {
            res.status(404).send('No such book found');
        }
    }).catch((err) => {
        res.status(400).send(err);
    });
});

app.delete('/book/:id', (req, res) => {
    Book.findByIdAndDelete(req.params.id).then((book) => {
        if (book) {
            res.status(200).send(book);
        } else {
            res.status(404).send('No book found');
        }
    }).catch(err => res.status(400).send(err));
});

app.listen(3000, () => {
    console.log('Up and running books service');
});