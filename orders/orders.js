const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const axios = require('axios');

app.use(bodyParser.json());

require('./order.model.js');
const Order = mongoose.model('orders');

mongoose.connect('mongodb+srv://parita-d:mongodbb@cluster0-bfv8j.mongodb.net/OrderService?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
        console.log('Connection established with OrderService');
    }).catch(() => {
        console.log('Connection failed');
    });

app.post('/order', (req, res) => {
    const newOrder = {
        customerId: mongoose.Types.ObjectId(req.body.customerId),
        bookId: mongoose.Types.ObjectId(req.body.bookId),
        initialDate: req.body.initialDate,
        deliveryDate: req.body.deliveryDate
    }
    const order = new Order(newOrder);
    order.save().then((data) => {
        if (data) {
            res.status(200).send(data);
        } else {
            res.status(400).send('Order could not be placed');
        }
    }).catch(err => res.send(err));
});

app.get('/orders', (req, res) => {
    Order.find().then((orders) => {
        if (orders) {
            res.status(200).send(orders);
        } else {
            res.send('No orders placed');
        }
    }).catch(err => res.send(err));
});

app.get('/order/:id', (req, res) => {
    Order.findById(req.params.id).then(async (order) => {
        if (order) {
            const orderObj = {
                id: order.id,
                initialDate: order.initialDate,
                deliveryDate: order.deliveryDate,
                customerName: '',
                bookTitle: ''
            };

            await axios.get('http://localhost:4000/customer/' + order.customerId)
                .then(customer => {
                    orderObj.customerName = customer.data.name;
                }).catch(() => res.send('Invalid customer id'));

            await axios.get('http://localhost:3000/book/' + order.bookId)
                .then(book => {
                    orderObj.bookTitle = book.data.title;
                }).catch(() => res.send('Invalid book id'));
            res.status(200).send(orderObj);
        } else {
            res.status(404).send('No order found with given id');
        }
    }).catch(err => res.send(err.message));
});

app.delete('/order/:id', (req, res) => {
    Order.findByIdAndDelete(req.params.id).then(order => {
        if (order) {
            res.status(200).send(order);
        } else {
            res.status(404).send('No such order to delete');
        }
    }).catch(err => res.send(err.message));
});

app.listen('5000', () => {
    console.log('Running order service');
})